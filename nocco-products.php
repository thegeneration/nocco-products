<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * @wordpress-plugin
 * Plugin Name: Nocco Products
 * Plugin URI: https://thegeneration.se/
 * Description: A Plugin for Nocco products
 * Version: 1.0.0
 * Author: The Generation AB
 * Author URI: https://thegeneration.se
 * Text Domain: nocco-products
 * Domain Path: languages
 */

/**
 * Define an absolute constant to be used in the plugin files
 */
if( ! defined( 'NOCCO_PRODUCTS' ) )
	define( 'NOCCO_PRODUCTS', __DIR__ );

if( ! defined( 'NOCCO_PRODUCTS' ) )
	define( 'NOCCO_PRODUCTS', __FILE__ );

/**
 * Require all the classes we need
 */
require_once NOCCO_PRODUCTS . '/inc/class-nocco-products-i18n.php';
require_once NOCCO_PRODUCTS . '/inc/class-nocco-products-scripts.php';
require_once NOCCO_PRODUCTS . '/inc/class-nocco-products-product.php';

/**
 * Initialize them
 */
(new Nocco_Products_i18n())->init();
(new Nocco_Products_Scripts())->init();
(new Nocco_Products_Product())->init();

$plugin_description = __( 'A Plugin for Nocco products', Nocco_Products_i18n::TEXT_DOMAIN );
$plugin_name = __( 'Nocco Products', Nocco_Products_i18n::TEXT_DOMAIN );