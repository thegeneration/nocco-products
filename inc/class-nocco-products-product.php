<?php

if( ! defined ( 'ABSPATH' ) ) exit; 

class Nocco_Products_Product {
		
	/**
     * Init function
     */
	public function init() {
        add_action( 'init', array ( $this, 'register_post_type' ) );
		add_action( 'init', array( $this, 'register_product_category' ) );
		add_action( 'product_category_edit_form_fields', array( $this, 'edit_product_category_fields' ), 10, 1 );
        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
        add_action( 'save_post', array( $this, 'save_meta_box_data' ) );
        add_shortcode( 'nocco_product_slider', array( $this, 'display_products' ) );
		add_shortcode( 'nocco_products', array( $this, 'display_products_by_category' ) );
	}

	/**
	* This function registers the post type
	* 
	* @return	void
	*/ 
	public function register_post_type() {

		$labels = array(
            'name'               => _x( 'Products', 'post type general name', Nocco_Products_i18n::TEXT_DOMAIN ),
            'singular_name'      => _x( 'Product', 'post type singular name', Nocco_Products_i18n::TEXT_DOMAIN ),
            'menu_name'          => _x( 'Products', 'admin menu', Nocco_Products_i18n::TEXT_DOMAIN ),
            'name_admin_bar'     => _x( 'Product', 'add new on admin bar', Nocco_Products_i18n::TEXT_DOMAIN ),
            'add_new'            => _x( 'Add New', 'product', Nocco_Products_i18n::TEXT_DOMAIN ),
            'add_new_item'       => __( 'Add New Product', Nocco_Products_i18n::TEXT_DOMAIN ),
            'new_item'           => __( 'New Product', Nocco_Products_i18n::TEXT_DOMAIN ),
            'edit_item'          => __( 'Edit Product', Nocco_Products_i18n::TEXT_DOMAIN ),
            'view_item'          => __( 'View Product', Nocco_Products_i18n::TEXT_DOMAIN ),
            'all_items'          => __( 'All Products', Nocco_Products_i18n::TEXT_DOMAIN ),
            'search_items'       => __( 'Search Products', Nocco_Products_i18n::TEXT_DOMAIN ),
            'parent_item_colon'  => __( 'Parent Product:', Nocco_Products_i18n::TEXT_DOMAIN ),
            'not_found'          => __( 'No products found.', Nocco_Products_i18n::TEXT_DOMAIN ),
            'not_found_in_trash' => __( 'No products found in Trash.', Nocco_Products_i18n::TEXT_DOMAIN )
        );

        $args = array(
            'labels'             => $labels,
            'public'             => false,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => _x( 'product', 'post slug', Nocco_Products_i18n::TEXT_DOMAIN ) ),
            'capability_type'    => 'page',
            'has_archive'        => false,
            'hierarchical'       => false,
            'menu_position'      => null,
            'menu_icon'          => 'dashicons-format-gallery',
            'supports'           => array( 'title', 'revision', 'thumbnail', 'page-attributes' )
        );

        register_post_type( 'nocco-product', $args );
	}
    
     /**
    * This function adds metaboxes to the image post type
    * 
    * @return   void
    */ 
    public function add_meta_boxes() {
        
        add_meta_box( 
            'info_metabox',
            __( 'Link',  Nocco_Products_i18n::TEXT_DOMAIN ),
             array( $this, 'link_metabox' ),
            'nocco-product',
            'side', 
            'low',
            array(
                'template_id'=> 'link-field',
                'title' => 'Add link: ',
                'nonce_field' => 'info_metabox_field',
                'nonce_nonce' => 'info_metabox_field_nonce',
            )
        );
        
        add_meta_box( 
            'product_meta_ingredient',
            __( 'Product ingredients', Nocco_Products_i18n::TEXT_DOMAIN ),
            array( $this, 'wysiwug_metabox' ),
                'nocco-product',
                'advanced', 
                'low',
                array(
                    'template_id'   => 'product-ingredients',
                    'title'         => 'Add ingredients: ',
                    'name'          => 'nocco_product_ingredient',
                    'name_'         => '_nocco_product_ingredient',
                    'nonce_field'   => 'product_ingredient_metabox_field',
                    'nonce_nonce'   => 'product_ingredient_metabox_field_nonce',
                )
        );
        
        add_meta_box( 
            'product_description',
            __( 'Product Description', Nocco_Products_i18n::TEXT_DOMAIN ),
            array( $this, 'wysiwug_metabox' ),
                'nocco-product',
                'advanced',
                'low',
                array(
                    'template_id'   => 'product-description',
                    'title'         => 'Add description: ',
                    'name'          => 'nocco_product_description',
                    'name_'         => '_nocco_product_description',
                    'nonce_field'   => 'product_description_metabox_field',
                    'nonce_nonce'   => 'product_description_metabox_field_nonce',
                )
        );
        
    }
    
    /**
    * This function creates dynamic content to Materials metabox, Dimension metabox and Other image info metabox
    * 
    * @param    WP_POST     $post 
    * @param    array       $metabox contains metabox id, title, callback, and args elements.
    * @return   void
    */
    public function link_metabox ($post, $metabox) {
        ?>
        <div class="wrap" id="<?php echo $metabox['args']['template_id'];?>-box">   
        <h3><?php _e( $metabox['args']['title'], Nocco_Products_i18n::TEXT_DOMAIN ); ?></h3>
        <?php
        
            wp_nonce_field( $metabox["args"]["nonce_field"], $metabox["args"]["nonce_nonce"] );
            
            $product_link = get_post_meta( $post->ID, '_product_link', true );

            if( !$product_link ) {
                $product_link = '';
            }
     
            ?>
        
            
            <label>
                <?php _e( 'Link: ' , Nocco_Products_i18n::TEXT_DOMAIN ); ?>
                <input name="product_link" type="text" value="<?php echo esc_attr( $product_link ); ?>" />
            </label>
        </div>
        <?php
        
    }
    
     /**
    * This function creates a metabox with a WYSIWUG editor
    * 
    * @param    WP_POST     $post 
    * @param    array       $metabox contains metabox id, title, callback, and args elements.
    * @return   void
    */
    public function wysiwug_metabox ($post, $metabox) {
      
        wp_nonce_field( $metabox['args']['nonce_field'], $metabox['args']['nonce_nonce'] );

        $product_text = get_post_meta( $post->ID, $metabox['args']['name_'] , true );

        if( ! isset( $product_text ) ) {
            $product_text = '';
        }

        wp_editor( $product_text, $metabox['args']['name'], array('textarea_rows' => '10') );
        ?>
        
    <?php
    }
    
    /**
    * This function saves the metadata from the metaboxes
    * 
    * @param    int     $post_id    The id of the term
    * @return   void
    */ 
    public function save_meta_box_data( $post_id ) {        
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        
        if( isset($_POST['post_type']) && $_POST['post_type'] !== 'nocco-product' ) {
            return;
        }
        
        if( isset( $_POST['info_metabox_field_nonce'] )
            && wp_verify_nonce( $_POST['info_metabox_field_nonce'], 'info_metabox_field' ) ) {
            
            update_post_meta( $post_id, '_product_link', $_POST['product_link']);
        }
        
        if( isset( $_POST['product_description_metabox_field_nonce'] )
            && wp_verify_nonce( $_POST['product_description_metabox_field_nonce'], 'product_description_metabox_field' ) ) {
            
            update_post_meta( $post_id, '_nocco_product_description', $_POST['nocco_product_description']);
        }
        
        if( isset( $_POST['product_ingredient_metabox_field_nonce'] )
            && wp_verify_nonce( $_POST['product_ingredient_metabox_field_nonce'], 'product_ingredient_metabox_field' ) ) {
            
            update_post_meta( $post_id, '_nocco_product_ingredient', $_POST['nocco_product_ingredient']);
        }
      
    }
	
	/**
	* This function saves the metadata for the product-category image to db
	* 
	* @param	int		$term_id	The id of the term
	* @return	void
	*/ 
	public function save_product_category_fields( $term_id ) {
	
		if ( isset( $_POST['term_meta'] ) ) {
			$term_meta = get_option( "taxonomy_" . $term_id );
			$cat_keys = array_keys( $_POST['term_meta'] );
			
			foreach ( $cat_keys as $key ) {
				if ( isset ( $_POST['term_meta'][$key] ) ) {
					$term_meta[$key] = $_POST['term_meta'][$key];
				}
			}
			
			update_option( "taxonomy_" . $term_id, $term_meta );
		}
	}
    
	public function display_products_by_category($atts) {
        
        $atts = shortcode_atts( array(
            "category_id"    => false
        ), $atts );

        $term = get_term_by('id', $atts['category_id'] , 'product_category');
			
		$args = array(
			'post_type'        => 'nocco-product',
			'post_status'      => 'publish',
			'orderby'          => 'menu_order',
			'order'            => 'ASC',
			'posts_per_page'   => -1,
			'tax_query' => array(
				array(
					'taxonomy'  => 'product_category',
					'field'     => 'id',
					'terms'     => $term->term_id,
				)
			)
		);
		ob_start();
		$query = new WP_Query( $args ); 
            
            $template = locate_template( 'nocco-products/product-category-item.php' );
            if ( $template == '' ) {
                $template = NOCCO_PRODUCTS . '/partials/product-category-item.php';
            }
            
            include ( $template );
        
		return ob_get_clean();
	}
	
	public function display_products() {
        ob_start(); ?>

        <div class="product-gallery owl-carousel">
            <?php
            $args = array(
                'post_type'        => 'nocco-product',
                'post_status'      => 'publish',
                'orderby'          => 'menu_order',
                'order'            => 'ASC',
                'posts_per_page'   => -1,
            );

            $query = new WP_Query( $args ); 

            $template = locate_template( 'nocco-products/product-carousel-item.php' );
            
            if ( $template == '' ) {
                $template = NOCCO_PRODUCTS . '/partials/product-carousel-item.php';
            }
            
            include ( $template );
            
            ?>
        </div>
        
		<?php
    	return ob_get_clean();
    }
	
	/**
	* This function registers custom categories for the product post type
	* 
	* @return	void
	*/ 
	public function register_product_category() {
        $labels = array(
            'name'                  => _x( 'Product Category', 'taxonomy general name', Nocco_Products_i18n::TEXT_DOMAIN  ),
            'singular_name'         => _x( 'Product Category', 'taxonomy singular name', Nocco_Products_i18n::TEXT_DOMAIN  ),
            'search_items'          =>  __( 'Search Categories', Nocco_Products_i18n::TEXT_DOMAIN  ),
            'all_items'             => __( 'All Categories', Nocco_Products_i18n::TEXT_DOMAIN  ),
            'parent_item'           => __( 'Parent Category', Nocco_Products_i18n::TEXT_DOMAIN  ),
            'parent_item_colon'     => __( 'Parent Category:', Nocco_Products_i18n::TEXT_DOMAIN  ),
            'edit_item'             => __( 'Edit Category', Nocco_Products_i18n::TEXT_DOMAIN  ),
            'update_item'           => __( 'Update Category', Nocco_Products_i18n::TEXT_DOMAIN  ),
            'add_new_item'          => __( 'Add New Category', Nocco_Products_i18n::TEXT_DOMAIN  ),
            'new_item_name'         => __( 'New Category Name', Nocco_Products_i18n::TEXT_DOMAIN  ),
            'menu_name'             => __( 'Categories', Nocco_Products_i18n::TEXT_DOMAIN  ),
        ); 
        
        register_taxonomy( 'product_category', 'nocco-product', array(
            'hierarchical'              => true,
            'labels'                    => $labels,
            'show_ui'                   => true,
            'show_admin_column'         => true,
            'query_var'                 => true,
            'rewrite'                   => array(
                'slug' => __( 'product-category', Nocco_Products_i18n::TEXT_DOMAIN )
            )
        ));
    }
}
