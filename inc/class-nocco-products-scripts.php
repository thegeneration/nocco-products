<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Nocco_Products_Scripts {

    /**
     * Init function
     */
    public function init() {
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_frontend_scripts' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_backend_scripts' ) );
    }

    /**
     * Enqueues scripts and styles for the frontend
     *
     * @return  void
     */
    public function enqueue_frontend_scripts() { 
        wp_enqueue_style( 'owl-carousel', plugins_url( 'assets/css/lib/owl-carousel/owl.carousel.css', __DIR__ ), false );
        wp_enqueue_style( 'nocco_products-frontend', plugins_url( 'assets/css/application.min.css', __DIR__ ), false, false );
        wp_enqueue_script( 'owl-carousel', plugins_url( 'assets/js/lib/owl-carousel/owl.carousel.min.js', __DIR__ ), array( 'jquery' ), true );
        wp_enqueue_script( 'nocco-products-frontend', plugins_url( 'assets/js/frontend/application.min.js', __DIR__ ), array('owl-carousel', 'jquery'), true );
    }

    /**
     * Enqueues scripts and styles for the backend
     *
     * @return  void
     */
    public function enqueue_backend_scripts() { }

}