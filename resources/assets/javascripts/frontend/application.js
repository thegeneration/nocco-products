jQuery(document).ready(function ($) {
    
    $('.product-gallery').each(function() {
		$(this).owlCarousel({
			loop: true,
			autoplay: false,
			items: 6,
            autoWidth: false,
			nav: true,
			responsive: {
				0: {
					items: 1
				},
                479: {
					items: 2
				},
				767: {
					items: 3
				},
                979: {
					items: 4
				},
                1225: {
                    items: 5
                },
				1450: {
					items: 6
				},
				1700: {
					items: 7
				}
			}
		});
        
    });
    
});