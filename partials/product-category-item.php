<?php if( ! defined ( 'ABSPATH' ) ) exit; 

if( isset( $query ) ) : ?>

 <table class="nocco-products-product-container">
    <?php while ( $query->have_posts() ) : 
        $query->the_post(); ?>
     
        <td>
            <?php the_post_thumbnail( 'medium', array( 'class' => 'nocco-product-item' ) ); ?>
        </td>
        
    <?php endwhile; wp_reset_postdata(); ?>
</table>


<?php endif; 
