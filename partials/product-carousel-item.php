<?php if( ! defined ( 'ABSPATH' ) ) exit; 

if( isset( $query ) ) :

    while ( $query->have_posts() ) : $query->the_post(); ?>
        <?php $product_link = get_post_meta( get_the_ID(), '_product_link', true );
        if( isset( $product_link ) && $product_link !== '' ) : ?>
            <a href="<?php echo $product_link; ?>" rel="gallery" class="product-gallery-img-container"><?php the_post_thumbnail( 'medium' );?></a>
        <?php else: ?>
            <p rel="gallery" class="product-gallery-img-container"><?php the_post_thumbnail( 'medium' );?></p>
        <?php endif; ?>
    <?php endwhile; wp_reset_postdata();

endif;
